import { TeamsActivityHandler, TurnContext, MessagingExtensionAction, MessagingExtensionActionResponse} from "botbuilder";

export interface DataInterface {
  likeCount: number
}

export class TeamsBot extends TeamsActivityHandler {
  public async handleTeamsMessagingExtensionFetchTask(
    context: TurnContext,
  ): Promise<MessagingExtensionActionResponse> {
    return empDetails(context);
  }
}

async function empDetails(context): Promise<MessagingExtensionActionResponse> {  
  return {
    task: {
      type: 'continue',
      value: {
        url: `https://localhost:53000/index.html#/custom_widget/`,
        width: 680,
        height: 400,
        title: 'Feedback & praise',
      }
    },
  };
}
