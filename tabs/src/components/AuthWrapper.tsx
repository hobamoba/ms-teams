import React, { useState } from "react";

import { Button } from "@fluentui/react-northstar";
import "./App.css";
import { useGraph } from "../hooks/useGraph";
import ContextBuilder from "../system/context-builder";
import GpdbClientWrapper from "./GpdbClientWrapper";

function AuthWrapper({context}: any) {
  const chatId =  context.chatId;
  const channelId = context.channelId;
  const [teamId, setTeamID] = useState(context.groupId);

  const getInteractionType = () => {
     if (chatId && chatId !== "") return "Chat";

     if (channelId && channelId !== "") return "Channel";

     return "Team";
  }

  const interactionType = getInteractionType();

  const [members, setMembers] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);

  const getCurrentUser = async (graph: any) => {
    const user = await graph.api("/me").get();
    let photoUrl;
    try {
      const photo = await graph.api("/me/photo/$value").get();
      photoUrl = URL.createObjectURL(photo);
    } catch(e) {
      console.log(e);
      photoUrl = "";
    }

    return { ...user, photoUrl}
  };

  const getChatMembers = async (graph: any) => await graph.api(`/chats/${chatId}/members`).get();

  const getTeamMembers = async (graph: any) => {
    const members = await graph.api(`/teams/${teamId}/members`).get();

    const photos = await graph.api(`/groups/${teamId}/photos`).get();
    console.log("PHOTOS", photos);

    return members;
  };

  const getAndSetCurrentTeamID = async (graph: any): Promise<void> => {
    const allTeams = await graph.api("me/joinedTeams").get();

    const currentTeam = allTeams.value.filter((item: any) => context.teamSiteDomain.includes(item.displayName))[0];

    setTeamID(currentTeam.id)
  }

  const getChannelMembers = async (graph: any) => {
    const members = await graph.api(`/teams/${teamId}/channels/${channelId}/members`).get();

    return members;
  }

  const { loading, error, data, reload } = useGraph(
    async (graph) => {
      try {
        if (context.channelType === "Private" || !teamId) getAndSetCurrentTeamID(graph);

        let _members;
        // getting user profile
        const currentUser = await getCurrentUser(graph);
        setCurrentUser(currentUser);
        console.log("currentUser", currentUser);

        // getting members depending on interaction type
        switch(interactionType) {
          case "Chat":
            const chatMembers = await getChatMembers(graph);
            _members = chatMembers;
            break;
          case "Channel":
            const channelMembers = await getChannelMembers(graph);
            _members = channelMembers;
            break;
          case "Team":
            const teamMembers = await getTeamMembers(graph);
            _members = teamMembers;
            break;
          default: 
            throw `Unexpected interaction type: ${interactionType}`;
        }
        
        setMembers(_members);

        const builder = new ContextBuilder();
        const _data = await builder.run(currentUser, _members?.value);

        return _data;
      } catch(e) {
        console.error(e);

        return undefined;
      }
    },
    //check this out more detailed, pay attension to the permissions json file(mb move this to that file?)
    { scope: ["User.Read", "ChatMember.Read", "TeamMember.Read.All", "Group.Read.All", "ChannelMember.Read.All", "Team.ReadBasic.All"] }
  );

  console.log("CONTEXT", context)
  return (
    <div>
      {!loading && !error && data?.configuration && <GpdbClientWrapper data={data} currentUser={currentUser} members={members} context={{...context, interactionType}}/>}

      {!loading && !data && (
        <div className="authorization__container">
          <p>NameCoach App requires authorization via MS365 accout in order to access to your profile data, team and chat members.</p>
          <Button primary content="Authorize" disabled={loading} onClick={reload} />
        </div>
      )}

      {!loading && error && <div>Something went wrong</div>}
    </div>
  );
}

export default AuthWrapper;
