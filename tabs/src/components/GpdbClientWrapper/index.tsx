import React, { useEffect, useState } from "react";
import {
  loadClient,
  PronunciationMyInfoWidget,
  SearchWidget,
} from "gpdb-widget";
import { Loader } from "@fluentui/react-northstar";
import TabContainer from "../TabContainer";

function GpdbClientWrapper({data, currentUser, members, context}: any) {
  console.log(context);
  const frameContext = context.frameContext;
  
  const isExtensionPopup = frameContext === "task";
  const isTab = frameContext === "content";

  const [loading, setLoading] = useState(true);
  const [client, setClient] = useState(
       loadClient(
        data.configuration,
        data.application,
        data.nameOwner,
        data.user,
        data.parser
  ));

  useEffect(() => {
    const load = async () => {
      setLoading(true);

      await client.loadPermissions({user_sig: data.user.signature});

      const _client = client;
      setClient(_client);
      setLoading(false);
    };
    
    load();
    // eslint-disable-next-line
  }, [data.names, data.name])
  
  const gpdbWidget = () => {
    return <div className="outlook-widget">
      <PronunciationMyInfoWidget client={client} name={data.name} names={data.names} />
      <SearchWidget client={client} />
    </div>;
  }

  return (
    <>
      {loading && <Loader />}
      {!loading && client.permissions && isExtensionPopup && gpdbWidget()}
      {!loading && client.permissions && isTab && <TabContainer client={client} data={data} currentUser={currentUser} members={members} context={context}/>}
    </>
  );
}


export default GpdbClientWrapper;