import React, { useState } from "react";
import { ExtensionWidget, PronunciationMyInfoWidget } from "gpdb-widget";
import { ProfileCard } from "../ProfileCard";
import "../App.css";
import Name, { NameTypes } from "gpdb-widget/build/types/resources/name";
import { Avatar, Flex, Text } from "@fluentui/react-northstar";

const TabContainer = ({client, data, currentUser, members, context}: any) => {
  console.log("TAB CONTAINER", client, data, currentUser)

  const membersPerPageCount = 5;
  const getDictionary = () => {
    switch (context.interactionType) {
      case "Chat":
        return { searchTitle: "Find a member of the chat", membersTitle: "Chat members"};
      case "Channel":
        return { searchTitle: "Find a member of the channel", membersTitle: "Channel members"};
      case "Team":
        return { searchTitle: "Find a member of the team", membersTitle: "Team members"};
      default:
        return { searchTitle: "Search", membersTitle: "People"};
    }
  };

  const dictionary = getDictionary();

  const profileData = {
    photoUrl: currentUser?.photoUrl,
    profile: {
      displayName: currentUser.displayName,
      mail: currentUser.mail,
      mobilePhone: currentUser?.businessPhones.length > 0 ? currentUser.businessPhones[0] : null,
    }
  }
  const membersData = members?.value
    .filter((member: any) => member?.email !== currentUser.mail)
    .sort((first: any, second: any) => first.displayName.localeCompare(second.displayName))
    .map((member: any) => {
      return {
        id: member.id,
        photoUrl: member.photoUrl,
        profile: {
          displayName: member.displayName,
          mail: member.email,
          mobilePhone: member?.businessPhones?.lenght > 0 ? member?.businessPhones[0] : null,
        }
      }
    });

  const [currentPageMembers, setCurrentPageMembers] = useState(membersData.slice(0,membersPerPageCount));
  const [nextMembersPresent, setNextMembersPresent] = useState(membersData.length > membersPerPageCount);
  const [previousMembersPresent, setPreviousMembersPresent] = useState(false);
  const [currentLastMemberIndex, setCurretLastMemberIndex] = useState(membersPerPageCount);

  const handleNextMembers = () => {
    setPreviousMembersPresent(true);

    const lastElementIndex = currentLastMemberIndex + membersPerPageCount;
    console.log("INDEX", lastElementIndex);
    setCurretLastMemberIndex(lastElementIndex)
    const _currentPageMembers = membersData.slice(currentLastMemberIndex, lastElementIndex);

    setCurrentPageMembers(_currentPageMembers);
    console.log("all", membersData);
    console.log("current", currentPageMembers);

    if (lastElementIndex > membersData.length) setNextMembersPresent(false);
  };
  const handlePreviousMembers = () => {
    if (currentLastMemberIndex === membersPerPageCount) {
      setPreviousMembersPresent(false);
    }

    if (!nextMembersPresent) setNextMembersPresent(true);

    const lastElementIndex = currentLastMemberIndex - membersPerPageCount;
    console.log("INDEX", lastElementIndex);

    setCurretLastMemberIndex(lastElementIndex)

    const _currentPageMembers = membersData.slice(lastElementIndex, currentLastMemberIndex);

    setCurrentPageMembers(_currentPageMembers);
    console.log("all", membersData);
    console.log("current", currentPageMembers);

  };

  const extensionWidgetNames: { [t in NameTypes]: Name } =
  {
    firstName: {key: 'Cole', type: 'firstName' as NameTypes.FirstName, exist: true},
    fullName: {key: 'Cole Cassidy', type: 'fullName' as NameTypes.FullName, exist: true},
    lastName: {key: 'Cassidy', type: 'lastName' as NameTypes.LastName, exist: false},
  };

  const [showMemberWidget, setShowMemberWidget] = useState(false);
  const [currentMember, setCurrentMember] = useState<{ [t in NameTypes]: Name }>(
    extensionWidgetNames
  );

  const selectCurrentMember = (member: any) => {
    
    const parsedNames = data.parser.parse(member.profile.displayName);
    console.log("display name", member.profile.displayName);
    console.log("NAMEEEEE", parsedNames);

    const extensionWidgetName =   {
    firstName: {key: parsedNames.firstName, type: 'firstName' as NameTypes.FirstName, exist: true},
    fullName: {key: parsedNames.fullName, type: 'fullName' as NameTypes.FullName, exist: true},
    lastName: {key: parsedNames.lastName, type: 'lastName' as NameTypes.LastName, exist: true},
  };


    console.log("CURRENT MEMBER", extensionWidgetName);

    setCurrentMember(extensionWidgetName);
    setShowMemberWidget(true);
  }

  const updateMembersList = (value: any) => {
    setShowMemberWidget(false);

    const _members = membersData.filter((item: any) => item.profile.displayName.toLowerCase().includes(value.toLowerCase()));

    if (_members.length === 1) selectCurrentMember(_members[0])
    if (_members.length <= membersPerPageCount) setNextMembersPresent(false);
    if (_members.length === membersData.length) setNextMembersPresent(true);

    setCurrentPageMembers(_members.slice(0, membersPerPageCount));
  }

  

//replace PronunciationMyInfoWidget with MyInfo later when sdk will have this import
  return (
    <div className="tab">
      <div className="search_box">
        <p>{dictionary.searchTitle}</p>
        <input
          aria-label="Search input field"
          className={"search_input"}
          type="text"
          required
          onChange={(e): void => updateMembersList(e.target.value)}
          onKeyPress={(e): void => console.log(e)}
        />
      </div>
      <div className="me_members">
        <div className="members__wrapper">
          <div className="members_header">
            <p>{dictionary.membersTitle}</p>

            <div>
              { previousMembersPresent && <div className="next_button" onClick={handlePreviousMembers}>&#8249;</div>}
              { nextMembersPresent && <div className="next_button" onClick={handleNextMembers}>&#8250;</div>}
            </div>
          </div>

          <div className="members__container">
            <div className="members">
              {currentPageMembers.map((member: any)=> {
                return <div key={member.id} className="profile__wrapper" onClick={() => selectCurrentMember(member)}><ProfileCard loading={false} data={member}/></div>
              })}
            </div>
          </div>
        </div>

        <div className="my_info__wrapper">
          <div>
            <p>Me</p>
            <Flex gap="gap.medium">
              <Avatar size="larger" image={profileData.photoUrl} name={profileData.profile.displayName} />{" "}
              <Flex column>
                <Text content={profileData.profile.displayName} weight="bold" />
                <Text content={profileData.profile.mail} size="small" />
                {profileData.profile.mobilePhone && <Text content={profileData.profile.mobilePhone} size="small" />}
              </Flex>
            </Flex>
          </div>

          <div className="pronunciation_my_info__wrapper">
            <PronunciationMyInfoWidget client={client} name={data.name} names={[]} />
          </div>

          {showMemberWidget && currentMember && <div key={Date.now()}className="member_widget">
              <ExtensionWidget client={client} names={currentMember}/>          </div>}
        </div>
      </div>
    </div>
  );
}

export default TabContainer;
