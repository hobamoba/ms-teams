import React, { useEffect, useState } from "react";

import * as microsoftTeamsSdk from "@microsoft/teams-js";
import AuthWrapper from "../AuthWrapper";

const TeamsSDKWrapper = () => {
  const [context, setContext] = useState<microsoftTeamsSdk.Context>();

  useEffect(
    () => {
      microsoftTeamsSdk.initialize();
      microsoftTeamsSdk.getContext((context) => setContext(context));
    }, []
  );

  return (
    <div>
      { context && <AuthWrapper context={context}/>}
    </div>
  );
}

export default TeamsSDKWrapper;
