import responceToCredentials, { Credentials } from "../types/credentials";

export default class ExtensionService {
    private readonly email: string;
    private readonly tool: string;
    private readonly serviceUrl: string | undefined;
    private readonly serviceToken: string | undefined;

    constructor(email: string, tool: string, serviceUrl?: string, serviceToken?: string) {
        this.email = email;
        this.tool = tool;

        this.serviceUrl = serviceUrl || process.env.SERVICE_API || "https://ext-api-beta.name-coach.com";
        this.serviceToken = serviceToken || process.env.SERVICE_TOKEN || "8094da617b09ec48e9c5";
        console.log("ENV!", process.env)
    }

    async getToken(): Promise<Credentials> {
        const response = await fetch(`${this.serviceUrl}/auth/credentials`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json;charset=utf-8",
                Accept: "application/json;charset=utf-8",
                Authorization: this.serviceToken,
            } as HeadersInit,
            body: JSON.stringify({email: this.email, tool: this.tool}),
        });

        try {
            const text = await response.text();
            const json = JSON.parse(text);

            if (response.ok) return responceToCredentials(json);
    
            throw await response.text();
        } catch(e) {
            console.log(e);
            throw e;
        }
    };

    getCredentials(): Promise<Credentials> {
        const accessKeyId = process.env.ACCESS_KEY_ID;
        const secretAccessKey = process.env.SECRET_ACCESS_KEY;
        
        if(accessKeyId && secretAccessKey && process.env.NODE_ENV !== "production") {
            return Promise.resolve({ accessKeyId, secretAccessKey });
        }

        return this.getToken();
    }
}
