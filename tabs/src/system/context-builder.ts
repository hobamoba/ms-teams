import ExtensionService from "../services/extension-service";
import Parser from "./parser";
import { Configuration } from "gpdb-widget";

const TOOL = "ms-teams";
const parser = new Parser();
const extSignature = (email: string) => email?.match(/@(.+)/)?.[1] || TOOL;

export default class ContextBuilder {
    async run(currentUser: any, members: any) {
        console.log(currentUser);
        console.log(members);
        //only display name can be left
        const currentUserName = currentUser?.displayName || `${currentUser.givenName} ${currentUser.surname}`;
        const currentUserEmail = currentUser?.mail || currentUser?.userPrincipalName;

        const userEmailDowncased = currentUserEmail.toLowerCase();

        // load gpdb config
        const extensionService = new ExtensionService (userEmailDowncased, TOOL);
        const credentials = await extensionService.getCredentials();

        console.log("CREDENTIALS", credentials);
        const configuration = {
            accessKeyId: credentials.accessKeyId,
            secretAccessKey: credentials.secretAccessKey,
            apiUrl: process.env.GPDB_API || "https://gpdb-beta.name-coach.com/api/public/v1",
            analyticsApiUrl: process.env.ANALYTICS_API,
        };

        // prepare props
        const name = {
            value: currentUserName,
            owner: { signature: currentUserEmail, email: currentUserEmail }
        };

        let names = [{
            key: "namecoach",
            value: "NameCoach",
            owner: { signature: "namecoach@namecoach.com", email: "namecoach@namecoach.com" }
        }];
        if (members)
            names = members.filter((member: { displayName: any; email: string; }) => member.displayName &&
                    member.email &&
                    member.email.toLowerCase() !== userEmailDowncased
                )
                .map((member: { email: string; displayName: any; userId: any; }) => {
                    const recipientEmailDowncased = member.email.toLowerCase();

                    return {
                        value: member.displayName,
                        key:  member.userId,
                        owner: { signature: recipientEmailDowncased, email: recipientEmailDowncased }
                    };
                });

        const applicationContext = {instanceSig: extSignature(userEmailDowncased), typeSig: "outlook-addin"};
        const userContext = {signature: userEmailDowncased, email: userEmailDowncased};
        //mmm check this out !!!!!!!!!!!!!!!!!!!!!!!
        const nameOwnerContext =  userContext;

        const appProps = {
            name,
            names,
            parser,
            configuration: new Configuration(configuration),
            application: applicationContext,
            nameOwner: nameOwnerContext,
            user: userContext
        };
    
        return {
            ...appProps,
            termsAndConditionsAccepted: true,
        };
    }
}  
