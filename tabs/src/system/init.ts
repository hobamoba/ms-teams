import { Configuration } from "gpdb-widget";
import Parser from "./parser";

const applicationContext = { instanceSig: '@outlook.com', typeSig: 'email_dns_name' }
const nameOwnerContext = { signature: 'bombaster666@outlook.com', email: 'bombaster666@outlook.comm' }
const userContext = { email: 'bombaster666@outlook.com', signature: 'bombaster666@outlook.com' }

const me = {
    value: 'Test test',
    owner: {
      signature: 'bombaster666@outlook.com',
      email: 'bombaster666@outlook.com'
    }
  };

  const email = {
    value: 'sometest.email@gmail.com',
    owner: {
      signature: 'sometest.email@gmail.com',
      email: 'sometest.email@gmail.com'
    }
  };
  const emailWithoutDot = {
    value: 'sometestemailwithoutadot@gmail.com',
    owner: {
      signature: 'sometestemailwithoutadot@gmail.com',
      email: 'sometestemailwithoutadot@gmail.com'
    }
  };
  const longEmailWithComplexDomain = {
    value: 'somelongemailwithcomplexdomain@custom-domain.omniemail.com',
    owner: {
      signature: 'somelongemailwithcomplexdomain@custom-domain.omniemail.com',
      email: 'somelongemailwithcomplexdomain@custom-domain.omniemail.com'
    }
  };
  const testName1 = {
    value: 'Jason Robertson',
    owner: {
      signature: 'jasonrobertson@gmail.com',
      email: 'jasonrobertson@gmail.com'
    }
  }
  const testName2 = {
    value: 'Robert John Downey Jr.',
    owner: {
      signature: 'robertdowney@gmail.com',
      email: 'robertdowney@gmail.com'
    }
  }
  const widgetTest = {
    value: 'widget test',
    owner: {
      signature: 'jon.snow@name-coach.com',
      email: 'jon.snow@name-coach.com'
    }
  }
  const names = [
    { key: 'snow', ...me },
    { key: 'key0', ...email },
    { key: 'key1', ...emailWithoutDot },
    { key: 'key2', ...longEmailWithComplexDomain },
    { key: 'key3', ...testName1 },
    { key: 'key4', ...testName2 },
    { key: "widget",  ...widgetTest },
  ]

export default function init() {
    const parser = new Parser();

    const configuration = {
        accessKeyId: "EreVt-DJUj7Q-fEw7LBLGidLtf85wBZG",
        secretAccessKey: "92cxG-3cXOYUgGx7GRw38CIyk_jA8LwFEzpgxsKur3M",
        apiUrl: "https://gpdb-beta.name-coach.com/api/public/v1",
        analyticsApiUrl: "https://analytics-api-staging.name-coach.com/api/v1/browser_extension",
    };


    // form return data
    const appProps = {
        name: me,
        names,
        parser,
        configuration: new Configuration(configuration),
        application: applicationContext,
        nameOwner: nameOwnerContext,
        user: userContext
    };

    return {
        ...appProps,
        termsAndConditionsAccepted: true,
    };
}
