import { NameParser, NPResult } from 'gpdb-widget'

const digitsRegexp = new RegExp(/[\d]+/);

const split = (name: string, regex: RegExp) => {
    const fullName = name.replace(digitsRegexp, "").replace(regex, " ");
    const parsedName = fullName.split(" ");
    const firstName = parsedName.slice(0, -1).join(" ").trim() || null;
    const lastName = parsedName[parsedName.length - 1] || null;

    return { firstName, lastName, fullName };
};

export default class Parser implements NameParser {
  parse(name: string): NPResult {
    if (!(name.includes(" ") || name.includes("@"))) return { firstName: null, lastName: null, fullName: name };

    if (name.includes("@")) {
        return split(name.split("@")[0], /\W|_/);
    }
    else {
        return split(name, /\s/);
    }
  }
}
