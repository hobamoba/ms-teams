export interface Credentials {
    accessKeyId: string;
    secretAccessKey: string;
}

export default function ({ access_key_id, secret_access_key }: { access_key_id: string, secret_access_key: string }): Credentials {
    return {
        accessKeyId: access_key_id,
        secretAccessKey: secret_access_key,
    };
}
