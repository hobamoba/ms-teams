import IFrontController from "gpdb-widget/build/types/front-controller";
import { NameOption } from "gpdb-widget/build/ui/components/FullNamesList";

export interface ScreenComponentProps {
    client: IFrontController;
    name: Omit<NameOption, "key">;
    names: NameOption[];
}
